// test.cpp
//   Derek Chiou
//     May 19, 2007

// STUDENTS: YOU ARE EXPECTED TO PUT YOUR TESTS IN THIS FILE, ALONG WITH PROC.CPP.

#include <stdio.h>
#include "types.h"
#include "generic_error.h"
#include "helpers.h"
#include "cache.h"
#include "test.h"

extern args_t args;
extern int addr_range;
extern cache_t **caches;

int last_proc,bar_flag,last_even_proc,store_done, proc_list[32] ;

test_args_t test_args;

void init_test() {
  switch(args.test) {
  case 0:
    test_args.addr_range = (int)(0.75*MEM_SIZE);
    break;

  case 1:
    test_args.addr_range = 768;
    last_proc = last_even_proc = store_done = 0;
    break;

  case 2:
    test_args.addr_range = MEM_SIZE*args.num_procs;
    break;
  case 3:
    test_args.addr_range = MEM_SIZE*args.num_procs;
    break;
  case 4:
    bar_flag = 0;   
    test_args.addr_range = MEM_SIZE*args.num_procs;
    break;
  case 5:
    test_args.addr_range = (int)(0.75*MEM_SIZE);
    break;
  default:
    test_args.addr_range = 768;
//    ERROR("don't recognize this test");
  }
}

void finish_test() {
  double hr;

  for (int i = 0; i < args.num_procs; ++i) {
    switch(args.test) {
    case 0:
      hr = caches[i]->hit_rate();
      if (!within_tolerance(hr, 0.5, 0.001)) {
	ERROR("out of tolerance");
      }
      break;
    case 1:   //ERROR prints in proc.cpp
       printf (" case 1 test passed\n");
       break ;  
    case 2:
       printf (" case 2 test passed\n");
       break ;  
    case 3:
       printf (" case 3 test passed\n");
       break ;  
    case 4:
       if(bar_flag == 1)
        printf (" case 4 test passed\n");
       else
        ERROR("BARRIER TEST failed\n");    
       break ;  
    case 5:
       for(int i =0; i < args.num_procs ; ++i)
       {
            if(proc_list[i] == 0)
                ERROR("TEST HANGED FAILURE\n");    
       }   
       printf (" case 5 test passed\n");
       break ;  
    default: 
      ERROR("don't recognize this test");
    }
  }
  printf("passed\n");
}
