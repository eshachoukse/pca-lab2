// proc.cpp
//   by Derek Chiou
//      March 4, 2007
// 

// STUDENTS: YOU ARE EXPECTED TO MODIFY THIS FILE TO INSERT YOUR
// TESTS.  HOWEVER, YOU MUST BE ABLE TO ACCEPT OTHER PROC.CPP FILES,
// AS I WILL BE REPLACING YOUR PROC.CPP WITH MINE (AND YOUR FELLOW
// STUDENTS') FOR TESTING PURPOSES.

// for 382N-10



#include <stdio.h>
#include <stdlib.h>
#include "generic_error.h"
#include "cache.h"
#include "proc.h"
#include "test.h"
#include "helpers.h"
#include <assert.h>

proc_t::proc_t(int __p) {
	proc = __p;
	init();
}

void proc_t::init() {
	response.retry_p = false;
	has_access = false;
	ld_p = false;
	new_cmd = 1;
	fresh = 0;
	assoc = 4; // TODO FIGURE  out a dynamic to way to set it to correct assoc
	iter = 0;
}

void proc_t::bind(cache_t *c) {
	cache = c;
}


// ***** FYTD ***** 

// this is just a simple random test.  I'm not giving
// you any more test cases than this.  You will be tested on the
// correctness and performance of your solution.

void proc_t::advance_one_cycle() {
	int data;
	static int count = 0;

	switch (args.test)
	{
	case 0:
		/*if (!response.retry_p) {
			addr = random() % test_args.addr_range;
			ld_p = ((random() % 2) == 0);
			if(ld_p)
				printf("Proc %d Generated LOAD at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
			else
				printf("Proc %d Generated STORE at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
		}
		if (ld_p) response = cache->load(addr, 0, &data, response.retry_p);
		else      response = cache->store(addr, 0, cur_cycle, response.retry_p);
		break;*/
		if (!response.retry_p)
		{
			do
			{
				addr = random() % test_args.addr_range;
			}while( gen_local_addr(addr) >= (int)(0.75*MEM_SIZE));

			ld_p = ((random() % 2) == 0);
			if(ld_p)
				printf("Proc %d Generated LOAD at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
			else
				printf("Proc %d Generated STORE at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
		}
		if (ld_p) response = cache->load(addr, 0, &data, response.retry_p);
		else      response = cache->store(addr, 0, cur_cycle, response.retry_p);
		break;

	case 1: // Multiple processors read and write on same address
		
		if (!response.retry_p) {
			if(proc %2)
				printf("Proc %d Generated LOAD at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
			else
				printf("Proc %d Generated STORE at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
		}
		if (proc == 3)
		{
			if( random() % 2)
            {   addr = 3;
                response = cache->load(addr, 0, &ld_value, response.retry_p);
                if (!response.retry_p){
                    if(!store_done)
                        break;

                    if(ld_value != last_proc){
                        printf("MATCHING addr 3 %d , last_proc %d \n",ld_value,last_proc);  
                        ERROR("FAILED READ-WRITE TEST for 3 \n");      
                    }     
                    else{
                        printf("MATCHING addr 3 %d , last_proc %d \n",ld_value,last_proc);  
                    } 
                }
            }
            else
            {   addr = 5;
                response = cache->load(addr, 0, &ld_value, response.retry_p);
                if (!response.retry_p){
                    if(!store_done)
                        break;
                    if(ld_value != last_even_proc){
                        printf("ERROR addr 5 %d , last_even_proc %d \n",ld_value,last_even_proc);  
                        ERROR("FAILED READ-WRITE TEST for 5 \n");      
                    }     
                    else{
                        printf("MATCHING addr 5 %d , last_even_proc %d \n",ld_value,last_even_proc);  
                    } 
                }
            }
		}
		else
		{
            if(proc%2)
            {   addr = 3;
                response = cache->store(addr, 0, proc, response.retry_p);
			    if (!response.retry_p)
                {
                    store_done = 1;
                    printf("WRITING addr 3 %d\n",proc);  
                    last_proc = proc;
                }
            }
            
            else
            {   addr = 5;
                response = cache->store(addr, 0, proc, response.retry_p);
			    if (!response.retry_p)
                {
                    printf("WRITING addr 5 %d\n",proc);  
                    store_done = 1;
                    last_even_proc = proc;
                }
            }
		}
		break;

	case 2: //CRITICAL SECTION
		//Global Address 200 = flag 1
		//Global Address 300 = flag 2
		//Shared Count is at global address 3ff
		if (!response.retry_p) {
			fresh = 1;

			switch(new_cmd) {
			case 1:
				//Store 1 to my flag
				ld_p = 0;
				if(!proc)
					addr = 0x200;
				else
					addr = 0x300;
				st_value = 1;
				new_cmd = 2;
				break;
			case 2:
				//Check other flag
				ld_p = 1;
				read_flag = 1;
				if(!proc)
					addr = 0x300;
				else
					addr = 0x200;
				new_cmd = 3;
				break;
			case 3:
				if(!ld_flag) { //In the CS
					ld_p = 1;
					addr = 0x3ff;
					new_cmd = 4;
					read_flag = 0;
				}
				else { //Didn't get the critical section, retry, till other flag is seen zero
					ld_p = 0;
					if(!proc)
						addr = 0x200;
					else
						addr = 0x300;
					if(!proc) new_cmd = 1;
					else new_cmd = 12;
					st_value = 0;
				}
				break;
			case 4:
				//Increment shared variable
				ld_p = 0;
				if(ld_value > 0)
					ERROR("FAILED: More than 1 processor in critical section\n");
				assert( ld_value == 0 );
				st_value = ld_value + 1;
				addr = 0x3ff;
				new_cmd = 5;
				break;
			case 5:
				ld_p = 1;
				addr = 0x07f;
				new_cmd = 6;
				read_flag = 0;
				break;

			case 6:
				ld_p = 1;
				addr = 0xff;
				new_cmd = 7;
				read_flag = 0;
				break;
			case 7:
				ld_p = 1;
				addr = 0x1ff;
				new_cmd = 8;
				read_flag = 0;
				break;
			case 8:
				ld_p = 1;
				addr = 0x2ff;
				new_cmd = 9;
				read_flag = 0;
				break;
			case 9:
				ld_p = 1;
				addr = 0x3ff;
				new_cmd = 10;
				read_flag = 0;
				break;
			case 10:
				//Decrement shared variable
				ld_p = 0;
				st_value = ld_value - 1 ;
				addr = 0x3ff;
				new_cmd = 11;
				break;

			case 11:
				//End of CS pull flag down
				ld_p = 0;
				if(!proc)
					addr = 0x200;
				else
					addr = 0x300;
				st_value = 0;
				new_cmd = 1;
				break;
			case 12:
				//NoP
				ld_p = 0;
				if(!proc)
					addr = 0x200;
				else
					addr = 0x300;
				st_value = 0;
				new_cmd = 1;
				break;

			default:
				assert(0);

			}

			if(ld_p)
				printf("Proc %d Generated LOAD at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
			else
				printf("Proc %d Generated STORE at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
		}
		if (ld_p) {
			if(read_flag)
				response = cache->load(addr, 0, &ld_flag, response.retry_p);
			else {
				response = cache->load(addr, 0, &ld_value, response.retry_p);
				assert(ld_value<2);
			}
		}
		else
		{
			response = cache->store(addr, 0, st_value, response.retry_p);
			if( (fresh) && (new_cmd == 11) )
				assert( response.retry_p );
		}
		fresh = 0;
		break;
		//command ./sim 16 100000 4 1 > log
	case 3:
		//Shared Count is at global address 3f2
		if (!response.retry_p) {
			fresh = 1;

			switch(new_cmd) {
			case 1:
				//all read a value
				ld_p = 1;
				addr = 0x3f2;  //same addres for all   - address must have all 1s in its most significant bits
				new_cmd = 2;
				break;
			case 2:
				//all proc stores into it
				ld_p = 0;
				st_value = proc;
				addr = 0x3f2; //same cache line
				new_cmd = 3;
				break;
			case 3:  // evict the cache line
				ld_p = 1;
				addr = addr  - (1<<(8- iter/2)); //iter runs from 0 to associativity -1
				if(iter == assoc-1)
				{
					new_cmd = 4;
					iter = 0;
				}
				else
				{
					new_cmd = 3;
					iter++;

				}
				break;
			case 4:  //after cache line is evicted
				//read it again
				ld_p = 1;
				addr = 0x3f2;
				new_cmd = 5;
				break;
			case 5: // check for read value
				if(ld_value != last_proc){

					printf("proc : %d  read_value %d, last_proc %d\n",proc,ld_value,last_proc);
					//assert(0);
					ERROR("Eviction test failed\n");
				}
				else{
					printf("MATCHING read_value %d, last_proc %d\n",ld_value,last_proc);

				}
				//Check for loaded value

				break;
			default:
				assert(0);
			}
		}

		if (!response.retry_p) {
			if(ld_p)
				printf("Proc %d Generated LOAD at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
			else
				printf("Proc %d Generated STORE at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
		}
		if (ld_p)
			response = cache->load(addr, 0, &ld_value, response.retry_p);
		else
		{
			response = cache->store(addr, 0, st_value, response.retry_p);
			if(!response.retry_p)
			{
				last_proc = proc ;
				printf("New last_proc %d \n",last_proc);
			}

		}
		break;

	case 4:
		// Barrier test
		// Shared location for barrier is 0x200
		// Shared variable is in 0x400
		if ( !response.retry_p )
		{
			switch(new_cmd)
			{

			case 1: // everyone tries to read the barrier located at 0x200
				ld_p = true;
				addr = 0x200;
				new_cmd = 2;
				break;

			case 2: // check the value of barrier
				if( ld_value == proc ) // processor can go forward, update the barrier value
				{
					ld_p = false;
					st_value = proc + 1;
					addr = 0x200;
					new_cmd = 3;
					printf( "Processor %d updated barrier to %d\n", proc, st_value );
				}
				else // processor should wait and should re-read the data.
				{
					ld_p = true;
					addr = 0x200;
					new_cmd = 2;
				}
				break;

			case 3: // passed the barrier, wait till everyone passes it.
				ld_p = true;
				addr = 0x200;
				new_cmd = 4;
				break;

			case 4: // check if it is correct.
				if( ld_value != args.num_procs )
				{
                    ld_p = true;
                    addr = 0x200;
                    new_cmd = 4;
				}
				else{
					ld_p = true;
					addr = 0x400;
					new_cmd = 5;
				}
				break;
			case 5:	
				printf("Processor: %d, crossed barrier, PASS\n", proc);
                bar_flag = 1;
				break;
			default:
				assert( 0 );
			}

			if(ld_p)
				printf("Processor %d Generated LOAD at addr = %x Owner is Processor %d\n", proc, addr, gen_node(addr) );
			else
				printf("Processor %d Generated STORE at addr = %x Owner is Processor %d\n", proc, addr, gen_node(addr) );
		}

		if (ld_p)
			response = cache->load(addr, 0, &ld_value, response.retry_p);
		else
		{
			response = cache->store( addr, 0, st_value, response.retry_p );
		}
		break;
	case 5:
		addr  = 3;
        if (!response.retry_p)
		{
				printf("Proc %d Generated STORE at addr = %x Owner is proc %d\n",proc, addr, gen_node(addr));
		}
        response = cache->store(addr, 0, proc, response.retry_p);
        if(!response.retry_p)
            proc_list[proc] = 1;
		break;

	default:
		ERROR("don't know this test case");
	}
}



