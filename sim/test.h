// test.h
//   Derek Chiou
//     May 19, 2007

void init_test();
void finish_test();

typedef struct {
  int addr_range;
} test_args_t;

extern int last_proc,bar_flag,store_done,last_even_proc,proc_list[32];  //for eviction test
extern test_args_t test_args;
