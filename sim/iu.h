// iu.h
//   by Derek Chiou
//      March 4, 2007
// 

// STUDENTS: YOU ARE ALLOWED TO MODIFY THIS FILE, BUT YOU SHOULDN'T NEED TO MODIFY MUCH, IF ANYTHING.
// for 382N-10

#ifndef IU_H
#define IU_H
#include "types.h"
#include "my_fifo.h"
#include "cache.h"
#include "network.h"

class send_packet_type {

public:
	net_cmd_t cmd;
	int src_port;
	pri_t pri;
	send_packet_type(net_cmd_t n, int s, pri_t p) {
		cmd = n;
		src_port = s;
		pri = p;
	}	
	send_packet_type() {
	}
};

typedef struct
{
	int last_src_port;
	pri_t last_type;
	my_fifo_t <send_packet_type> *send_queue;
	my_fifo_t <send_packet_type> *reply_send_queue;
	net_cmd_t last_net_cmd;
	net_cmd_t last_ISC_cmd;
	bool self_retry;
	bool retry;
	int retry_count;
	int last_inv_time;
	address_t last_inv_tag; 

	int ISC;
	bool broadcast;

	bool cmd_not_taken_first_time;
} iu_status;

class dir_entry {
	/* --- BITS OF THE ENTRY ---
	 * [31] = inv_acks_waiting
	 * [30] = dirty
	 * [29] = self-ownership
	 * [28] = free to use
	 * [27:25] = counter
	 * [24:20] = destination
	 * [19:15], [14:10], [9:5], [4:0] = four owners */
	public:
	int entry;

	int get_inv_acks_waiting() { return ( (entry & 0x80000000) >> 31 ); }
	int get_dirty() { return ( (entry & 0x40000000) >> 30 ); }
	int get_self_ownership() { return ( (entry & 0x20000000) >> 29); }
	int get_counter() { return ( (entry & 0x0E000000) >> 25 ); }
	int get_destination() { return ( (entry & 0x01F00000) >> 20 ); }

	dir_entry( int e ) { this->entry = e; }

	int get_owner( int idx )
	{
		if(idx > 3)
			return -1;
		return ( (entry & (0x000F8000 >> (idx * 5))) >> ((3-idx) * 5) ); 
	}

	void set_inv_acks_waiting( int iaw )
	{
		int temp = (entry & 0x7FFFFFFF) | (iaw << 31);
		entry = temp;
	}

	void set_dirty( int d )
	{
		int temp = (entry & 0xBFFFFFFF) | (d << 30);
		entry = temp;
	}

	void set_self_ownership( int s )
	{
		int temp = (entry & 0xDFFFFFFF) | (s << 29);
		entry = temp;
	}

	void set_counter( int c )
	{
		int temp = (entry & 0xF1FFFFFF) | (c << 25);
		entry = temp;
	}

	void increment_counter()
	{
		int temp = get_counter();
		set_counter( temp + 1 );
	}

	void set_destination( int d )
	{
		int temp = (entry & 0xFE0FFFFF) | (d << 20);
		entry = temp;
	}

	void set_owner( int idx, int o )
	{
		int temp = (entry & ~(0x000F8000 >> (idx * 5))) | (o << ( (3 - idx) * 5)) ;
		entry = temp;
	}
	
	bool is_owner( int o )
	{
		if( get_counter() < 5 )
		{
			for(int i=0; i < get_counter(); i++) {
				if(get_owner(i) == o)
					return true;
			}
			return false;
		}
		return true;
	}
	void add_owner( int o )
	{
		if( get_counter() < 5 )
		{
			//Don't add owner if already in the list!
			for(int i=0; i < get_counter(); i++) {
				if(get_owner(i) == o)
					return;
			}
			//Can't add anymore owners
			if(get_counter() == 4) {
				set_counter( 5 );
				return;
			}
			set_owner( get_counter(), o );
			set_counter( get_counter() + 1 );
		}
		else
		{
			set_counter( 5 );
		}
	}

	void decrement_counter()
	{
		int temp = get_counter();
		set_counter( temp - 1 );
	}
};

class iu_t {
	int node;

	int local_accesses;
	int global_accesses;
    int mem_access ;
	/*[Ali]: All of these moved to my_status*/

	//net_cmd_t last_net_cmd; //TODO save in to_net function
	//int isc; // keeps a count of how many isc have to be sent
	// since this value is used as index , isc < 0 means no more invalidate to send 
	//bool broadcast; // if we are in the process of sending invalid bits , then this bit tells if /
	// we are broadcasting or sending individual invalidates

	data_t mem[ MEM_SIZE ];

	cache_t *cache;
	network_t *net;

	bool proc_cmd_p;
	proc_cmd_t proc_cmd;

	bool proc_cmd_processed_p;

	// processor side
	bool process_proc_request(proc_cmd_t proc_cmd);

	// network side
	bool process_net_request(net_cmd_t net_cmd);
	bool process_net_reply(net_cmd_t net_cmd);

	bus_tag_t timestamp;

	public:
	iu_t(int __node);

	iu_status my_status;
	void bind(cache_t *c, network_t *n);

	void advance_one_cycle();
	void print_stats();

	// processor side
	bool from_proc(proc_cmd_t pc);
	bool from_proc_overwrite(proc_cmd_t pc);
	// network side
	bool from_net(net_cmd_t nc);
	bool to_net();
	bool check_retry(net_cmd_t net_cmd);
	void send_retry( int dest, proc_cmd_t proc_cmd );
	void send_ack( int dest, proc_cmd_t proc_cmd );
	void send_command(int dest, proc_cmd_t proc_cmd);
	void broadcast_invalidate(proc_cmd_t pc);
	void send_invalidate(proc_cmd_t cmd);
	void self_retry( void );

	void read_cache_line(address_t addr, cache_access_response_t car, data_t return_line);
	void print_cache_line(address_t addr, cache_access_response_t car);
	/*[Ali]: Addresses to 768-1023 should be mapped to Directory*/
	dir_entry get_dir_entry( int addr );
	void set_dir_entry( int addr, int new_dir_ent );
};
#endif
