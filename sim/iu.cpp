// iu.cpp
//   by Derek Chiou
//      March 4, 2007
// 

// STUDENTS: YOU ARE EXPECTED TO MAKE MOST OF YOUR MODIFICATIONS IN THIS FILE.
// for 382N-10

#include <stdlib.h>
#include "types.h"
#include "helpers.h"
#include "my_fifo.h"
#include "cache.h"
#include "iu.h"
#include <assert.h> 

iu_t::iu_t(int __node)
{
	node = __node;
	for (int i = 0; i < MEM_SIZE; ++i)
		for (int j = 0; j < CACHE_LINE_SIZE; ++j)
			mem[i][j] = 0;

	my_status.self_retry = false;
	my_status.ISC = -1;
	my_status.broadcast = false;
	my_status.cmd_not_taken_first_time = false;
	timestamp = 0;
	mem_access = 0;
	my_status.send_queue = new my_fifo_t <send_packet_type> (2*args.num_procs);
	my_status.reply_send_queue = new my_fifo_t <send_packet_type> (args.num_procs);
	my_status.retry = 0;
	my_status.retry_count = 0;
	my_status.last_inv_time = NETWORK_LATENCY;
}

dir_entry iu_t::get_dir_entry( int addr )
{
	mem_access++ ;
	assert( gen_local_addr(addr) < (int)(0.75*MEM_SIZE));
	int cache_line_addr = gen_local_cache_line( addr );
	int new_dir_ent = mem[ cache_line_addr / 4 + (int)(0.75*MEM_SIZE) ][ cache_line_addr % 4 ];
	dir_entry temp (new_dir_ent);
	NOTE_ARGS(("NODE %d::: DIR at mem[%d][%d] dirty: %d Count: %d self_owner: %d owner0: %d dest: %d IAW: %d \n", \
				node, cache_line_addr / 4 + (int)(0.75*MEM_SIZE), cache_line_addr % 4, temp.get_dirty(), \
				temp.get_counter(), temp.get_self_ownership(), temp.get_owner(0) , temp.get_destination(), temp.get_inv_acks_waiting()));
	return dir_entry(new_dir_ent);
}

void iu_t::set_dir_entry( int addr, int new_dir_ent )
{
	mem_access++ ;
	assert( gen_local_addr(addr) < (int)(0.75*MEM_SIZE));
	int cache_line_addr = gen_local_cache_line( addr );
	mem[ cache_line_addr / 4 + (int)(0.75*MEM_SIZE) ][ cache_line_addr % 4 ] = new_dir_ent;
	dir_entry temp (new_dir_ent);
	NOTE_ARGS(("NODE %d::: DIR at mem[%d][%d] dirty: %d Count: %d self_owner: %d owner0: %d dest: %d IAW: %d \n", \
				node, cache_line_addr / 4 + (int)(0.75*MEM_SIZE), cache_line_addr % 4, temp.get_dirty(), \
				temp.get_counter(), temp.get_self_ownership(), temp.get_owner(0) , temp.get_destination(), temp.get_inv_acks_waiting()));
}

void iu_t::bind(cache_t *c, network_t *n)
{
	cache = c;
	net = n;
}


void iu_t::advance_one_cycle() {  
	timestamp++;
	mem_access = 0;
	my_status.last_inv_time++;
	// fixed priority: reply from network
	/*if( my_status.self_retry )
	  {
	  NOTE_ARGS(("node: %d , self_retry\n", node));
	  self_retry();
	  }*/
	//else 
	if (net->from_net_p(node, REPLY))
	{
		NOTE_ARGS(("node: %d , process_net_reply\n", node));
		/* check if reply does not require a send, we can process a send also
		 * like when we receive a inv ack, we can also send another inv, if we are HS
		 * process_net_reply returns a bool, we can use that for this.*/

		if( !process_net_reply( net->from_net(node, REPLY) ) )
		{
			if ( my_status.ISC >=0 ) // continue if ISC!
			{	
				if( my_status.broadcast )
					broadcast_invalidate( my_status.last_ISC_cmd.proc_cmd ); // the pc is obtained from the iu_response
				// structure
				else
					send_invalidate( my_status.last_ISC_cmd.proc_cmd );
			}
		}
	}
	else if ( my_status.ISC >=0 ) // continue if inv still left to send
	{
		NOTE_ARGS(("node: %d , ISC non zero\n", node));
		if( my_status.broadcast )
			broadcast_invalidate( my_status.last_ISC_cmd.proc_cmd ); // the pc is obtained from the iu_response structure
		else
			send_invalidate( my_status.last_ISC_cmd.proc_cmd );
	}    
	else if(net->from_net_p(node, REQUEST))
	{  
		NOTE_ARGS(("node: %d , process_net_request\n", node));
		if(!process_net_request( net->from_net(node, REQUEST) ))
			if(proc_cmd_p && !proc_cmd_processed_p)
			{
				proc_cmd_processed_p = true;
				process_proc_request(proc_cmd);
			}  
	}        
	else if (proc_cmd_p && !proc_cmd_processed_p)
	{
		NOTE_ARGS(("node: %d , process_proc_request\n", node));
		proc_cmd_processed_p = true;
		process_proc_request(proc_cmd);
	}
	else{
		NOTE_ARGS(("node: %d , did nothing\n", node));
		//assert(0);
		//Nothing 
	}

	to_net();

	NOTE_ARGS(("mem_access %d \n",mem_access ));
	assert(mem_access <= 6);
}

// processor side

// this interface method only takes and buffers a request from the
// processor.
void iu_t::read_cache_line(address_t addr, cache_access_response_t car, data_t return_line ) {

	for(int i=0; i<CACHE_LINE_SIZE; i++) {
		address_t temp = addr >> LG_CACHE_LINE_SIZE;
		temp = (temp << LG_CACHE_LINE_SIZE) | i;
		return_line[i] = cache->read_data( temp, car);
	}
}

void iu_t::print_cache_line(address_t addr, cache_access_response_t car ) {

	//printf("print cache data ");
	for(int i=0; i<CACHE_LINE_SIZE; i++) {
		address_t temp = addr >> LG_CACHE_LINE_SIZE;
		temp = (temp << LG_CACHE_LINE_SIZE) | i;
		//printf("%d", cache->read_data( temp, car));
	}
	//printf("\n");
}
bool iu_t::from_proc(proc_cmd_t pc)
{
	if (!proc_cmd_p) {
		proc_cmd_p = true;
		proc_cmd = pc;
		proc_cmd.tag = timestamp;

		proc_cmd_processed_p = false;
		return(false);
	} else {
		assert(0);
		return(true);
	}
}

bool iu_t::from_proc_overwrite(proc_cmd_t pc)
{
	proc_cmd_p = true;
	proc_cmd = pc;
	proc_cmd.tag = timestamp;
	proc_cmd_processed_p = false;
	return(false);
}

bool iu_t::process_proc_request(proc_cmd_t pc)
{
	int dest = gen_node(pc.addr);
	int lcl = gen_local_cache_line(pc.addr);

	NOTE_ARGS(("%d: addr = %x, dest = %d", node, pc.addr, dest));

	if(my_status.last_inv_time < NETWORK_LATENCY) {
		if(my_status.last_inv_tag == cache->gen_address_tag(pc.addr)) {
			proc_cmd_processed_p = false;
	        NOTE_ARGS(("last_inv_time < network latency"));
			return false;
		}
	}
	if(my_status.retry) {
	    NOTE_ARGS(("status.retry"));
		my_status.retry_count--;
		if(!my_status.retry_count)
			my_status.retry = false;
		else {
			proc_cmd_processed_p = false;
			return false;
		}
		
	}

	if (dest == node) // local
	{
		dir_entry DE = get_dir_entry( pc.addr );

		//Retry if invalid_acks_waiting on this cache line
		if(DE.get_inv_acks_waiting()) {
			proc_cmd_processed_p = false;
			my_status.retry = true;
			my_status.retry_count = random()%(32*NETWORK_LATENCY);
			return(false);
		}
		++local_accesses;
		//proc_cmd_p = false; // clear proc_cmd

		switch(pc.busop)
		{
			case READ:
				{
					//Processor encountered a load miss
					if(!DE.get_dirty()) {
						//Got a self read request on clean data
						//Just read from memory
						DE.set_self_ownership(1);
						set_dir_entry(pc.addr, DE.entry);
						proc_cmd_p = false;	
						copy_cache_line(pc.data, mem[lcl]);
						mem_access = mem_access+1 ;
						cache->reply(pc);
						return(false);
					}
					else {
						//Got a self read request on dirty data, send INVALIDATE
						DE.set_inv_acks_waiting(1);
						DE.set_destination(node);
						assert(!DE.get_counter());
						assert(!DE.get_self_ownership());
						pc.busop = INVALIDATE;
						send_command(DE.get_owner(0), pc);
						set_dir_entry(pc.addr, DE.entry);
						return(false);
					}
				}

			case WRITE:
				{

					//I am evicting my cache line to 
					//HS<---- INVmemory

					//Make sure that the cache line is NOT already invalidated
					//If it was already invalidated, I should have overwritten 
					//the proc_cmd with a non-WB command
					cache_access_response_t car;
					assert(cache->cache_access(pc.addr, MODIFIED, &car));
					copy_cache_line(mem[lcl], pc.data);
					mem_access++ ;
					DE.set_self_ownership(0);
					DE.set_dirty(0);
					set_dir_entry(pc.addr, DE.entry);	
					cache->reply(pc);
					proc_cmd_p = false;
					return(false);  // need to return something for now
				}

			case INVALIDATE:
				{
					//Processor encountered a Store miss
					if(!DE.get_dirty()) {
						if(!DE.get_counter()) {
							//Nobody has the data, just read it from the memory
							DE.set_dirty(1);
							DE.set_self_ownership(1);
							set_dir_entry(pc.addr, DE.entry);
							proc_cmd_p = false;
							copy_cache_line(pc.data, mem[lcl]);
							mem_access++ ;
							cache->reply( pc);
							return(false);
						}
						else {
							//Somebody else has the data in Shared, invalidate them
							DE.set_inv_acks_waiting(1);
							DE.set_destination(node);
							if(DE.get_counter() == 5) //first time of broadcast
							{
								my_status.ISC = args.num_procs - 1;  // list goes from 0 - num-1, ISC value is used as index 
								DE.set_owner(3, args.num_procs - 1);
								set_dir_entry( pc.addr, DE.entry );
								my_status.broadcast = true;
								broadcast_invalidate(pc); 
							}
							else  //first time selective invalidate
							{  
								my_status.broadcast = false;				
								my_status.ISC = DE.get_counter() - 1; // list goes from 0 - num-1 
								set_dir_entry( pc.addr, DE.entry );
								send_invalidate(pc);
							}
							return(false);

						}
					}
					else {
						//Someone else modified the data, ask them to WB and invalidate
						assert(!DE.get_self_ownership());
						assert(!DE.get_counter());
						DE.set_inv_acks_waiting(1);
						DE.set_destination(node);
						set_dir_entry(pc.addr, DE.entry);
						pc.busop = INVALIDATE;
						send_command(DE.get_owner(0), pc);
						return(false);			
					}
				}

		}
	}
	else // global
	{

		//Make sure that the cache line is NOT already invalidated
		//If it was already invalidated, I should have overwritten 
		//the proc_cmd with a non-WB command
		if(pc.busop == WRITE) {
			cache_access_response_t car;
			assert(cache->cache_access(pc.addr, SHARED, &car));
		}
		++global_accesses;

		send_command( dest, pc);
		return(false);
	}
}


// receive a net request
bool iu_t::process_net_request(net_cmd_t net_cmd) {
	proc_cmd_t pc = net_cmd.proc_cmd;
	int lcl = gen_local_cache_line(pc.addr);
	int src = net_cmd.src;
	int dest = net_cmd.dest;
	cache_access_response_t car;


	NOTE_ARGS(("%d: addr = %x, dest = %d,src = %d", node, pc.addr, dest,src));
	if(gen_node(pc.addr) == node) //  I am HS
	{
		if(check_retry(net_cmd)) // retry check based on IAW ,return 1 for retry
        {
            NOTE_ARGS(("Sent retry \n"));
            return true;
        }
	}
	if(pc.busop == WRITE)   //check for retry request received 
	{ 
		if(gen_node(pc.addr) != node) // this WRITE request is a retry
		{
			//Could be a retry from an older WB request 
			//Which was already invalidated resulting in overwrite of proc_cmd
			//Now, it does not need to be retried at all
			if(pc.tag == proc_cmd.tag) 
			{  
				//process_proc_request(proc_cmd); 
				//return true;	
				proc_cmd_processed_p = false;
				my_status.retry = true;
				my_status.retry_count = random()%(32*NETWORK_LATENCY);
				//my_status.retry_count = (args.num_procs - node)*NETWORK_LATENCY;
				return false;
			}
			else{
				//TODO remove this: checks invalidate during WB
				//if(pc.permit_tag == INVALID)
				//	assert(0);
			}
			return false;
		}
	}
	if((pc.busop == READ || pc.busop == WRITE) && !(gen_node(pc.addr) == node))
		assert(0);

	dir_entry DE = get_dir_entry( net_cmd.proc_cmd.addr );
	switch(pc.busop)
	{
		case READ:
			//READ req => I am the HS
			if( !DE.get_dirty() )
			{
				// putting the data in the ACK
				copy_cache_line( net_cmd.proc_cmd.data, mem[lcl] );
				mem_access++ ;
				// sending the ACK
				send_ack( net_cmd.src,net_cmd.proc_cmd ); 
				DE.add_owner( net_cmd.src );
				// updating the directory
				set_dir_entry( pc.addr, DE.entry );
			}
			else   // cache line is dirty
			{
				// setting the destination to the person we are sending the
				// write request to.
				DE.set_destination( net_cmd.src );

				if( DE.get_self_ownership())// HS (myself) is modifier
				{
					//Assert that I am infact in modified state
					assert(cache->cache_access( pc.addr, MODIFIED, &car));
					data_t cache_line;
					read_cache_line( pc.addr, car, cache_line);
					print_cache_line( pc.addr, car);
					copy_cache_line( mem[lcl], cache_line); 
					mem_access++ ;
					//Wrote back the data, go to shared since this was a READ req
					cache->modify_permit_tag(car,SHARED);
					copy_cache_line( net_cmd.proc_cmd.data, cache_line); // read the updated cache line
					assert(DE.get_counter() == 0); // count should be equal to 0
					DE.add_owner(net_cmd.src); 					    
					DE.set_dirty(0);
					//DE.set_self_ownership(0);    
					send_ack(net_cmd.src,net_cmd.proc_cmd); 
					set_dir_entry( pc.addr, DE.entry );
				}
				else  // someone else is modifier
				{
					// create an invalidate request to modifier
					assert(DE.get_counter() == 0);
					net_cmd.proc_cmd = pc;
					net_cmd.proc_cmd.busop = INVALIDATE;
					send_command(DE.get_owner(0),net_cmd.proc_cmd);
					DE.set_inv_acks_waiting(1);
					set_dir_entry( pc.addr, DE.entry );
				}
			}
			return true;

		case WRITE:// based on tags it can be normal writeback if I am the HS or a retry from HS for my earlier request
			if(gen_node(pc.addr) == node) // normal write back request and I am HS
			{
				if(DE.get_dirty()) //line must be dirty
				{
					//An old request from a modifier that is now already in 
					//INVALID / SHARED state 
					//The requester is no longer waiting on this proc request 
					//Since it was already invalidated
					//
					//HS<---- INV
					//modifier --- WB--INV_ACK-->HS
					//we receive a late WB afte the 
					if((DE.get_self_ownership()) | (DE.get_owner(0) != net_cmd.src)) {//late writeback
						//DO NOTHING
						NOTE_ARGS(("node: %d Received late WB on address %x \n", node, pc.addr));
						//assert(0); DON'T ADD ASSERT - VALID STATE
						return false;
						//send_ack(net_cmd.src, net_cmd.proc);	
					}
					//assert((DE.get_counter() == 0)&& !DE.get_self_ownership());
					assert(DE.get_counter() == 0);
					copy_cache_line(mem[lcl], pc.data);
					mem_access++ ;
					DE.set_dirty(0);    
					send_ack(net_cmd.src, net_cmd.proc_cmd);
					set_dir_entry( pc.addr, DE.entry );
				}
				//Received a late WB, DO NOTHING  
				else {

					NOTE_ARGS(("node: %d Received WB(late)  %x but our implementation    \
								should not allow it\n", node, pc.addr));
					assert(0);

				}
			}
			else
			{
				// RETRY taken care  before the switch case
				assert(0);
			}
			return true;
		case INVALIDATE:
			// can receive invalidate in 2 situations
			// 1. I am HS and someone wants write permissions
			// 2. HS sends me instructions to invalidate the cache line

			NOTE_ARGS(("node: %d Received Invalidate on address %x HS is %d\n", node, pc.addr, gen_node(pc.addr)));
			if(gen_node(pc.addr)  == node) // invalidate request (someone wants write permissions) and I am HS
			{
				if(DE.get_dirty())  // cache line is dirty, need to ask the modifier to INV/WB
				{
					NOTE_ARGS(("node: %d Invalidate on dirty line Me_mod:%d Modifier:%d\n", node, DE.get_self_ownership(), DE.get_owner(0)));
					assert(DE.get_counter() == 0);
					if(!DE.get_self_ownership())  // I am not the modifier
					{   
						// create an invalidate request to modifier
						DE.set_destination(net_cmd.src);
						pc.busop = INVALIDATE;
						send_command(DE.get_owner(0),pc); 
						DE.set_inv_acks_waiting(1); 
						set_dir_entry( pc.addr, DE.entry );
					}
					else // HS(myself) is the modifier
					{
						//Assert that I am infact in modified state
						assert(cache->cache_access( pc.addr, MODIFIED, &car));
						data_t cache_line;
						read_cache_line( pc.addr, car, cache_line);
						print_cache_line( pc.addr, car);
						copy_cache_line( mem[lcl], cache_line); 
						mem_access++ ;

						//Wrote back the data, go to shared since this was a READ req
						cache->modify_permit_tag(car, INVALID);
						copy_cache_line(pc.data, cache_line); // read the updated cache line

						// never resetting the dirty bit bcoz it was RWITM
						DE.set_self_ownership(0);
						DE.set_owner(0,net_cmd.src);
						NOTE_ARGS(("node: %d Sending ACK to invalidater\n", node));
						send_ack(net_cmd.src ,pc);    
						set_dir_entry( pc.addr, DE.entry );
					}
				}
				else if((DE.get_counter() > 0) && ((DE.get_counter() != 1) | (DE.get_owner(0)!=net_cmd.src)))  // it is shared
				{

					NOTE_ARGS(("node: %d Invalidate on shared line\n", node));
					DE.set_destination(net_cmd.src);
					if(DE.get_self_ownership())
					{ 
						if(cache->cache_access( pc.addr, SHARED, &car))	
							cache->modify_permit_tag(car, INVALID);
						DE.set_self_ownership(0);
					}
					DE.set_inv_acks_waiting(1); 
					if(DE.get_counter() == 5) //first time of broadcast 
						//count has a corner case Does it include the count for myself and requester ??
					{
						// Don't send invalidate to urself and the requester
						my_status.broadcast = true;
						my_status.ISC = args.num_procs - 1;  // list goes from 0 - num-1, ISC value is used as index 
						if(node != net_cmd.src)
							DE.set_owner(3, args.num_procs - 2);
						else
							DE.set_owner(3, args.num_procs - 1);
						set_dir_entry( pc.addr, DE.entry );
						broadcast_invalidate(pc); 
					}
					else  //first time selective invalidate
					{  
						// Don't send invalidate to urself and the requester
						my_status.broadcast = false;	
						my_status.ISC = DE.get_counter() - 1; // list goes from 0 - num-1 
						if(DE.is_owner(net_cmd.src)) 	// Subtract one from acks_to_be_recvd if requester is an owner
							DE.set_counter(my_status.ISC);					
						set_dir_entry( pc.addr, DE.entry );
						send_invalidate(pc);
					}
				}
				else if(DE.get_self_ownership())  //cache line is only with HS (and maybe requester) in shared mode
				{

					NOTE_ARGS(("node: %d Invalidate on a line owned only by me : clean\n", node));
					if(cache->cache_access( pc.addr, SHARED, &car))	
						cache->modify_permit_tag(car, INVALID);
					copy_cache_line(pc.data,mem[lcl]); // read the cache line from memory
					mem_access++ ;
					DE.set_self_ownership(0);
					DE.set_owner(0,net_cmd.src);
					send_ack(net_cmd.src ,pc);
					DE.set_dirty(1);     
					DE.set_counter(0);
					set_dir_entry( pc.addr, DE.entry );
				}
				else  
				{  
					//Maybe only the requester has it in shared
					NOTE_ARGS(("node: %d Invalidate on unshared clean line\n", node));
					//cache line is clean and not shared
					copy_cache_line(pc.data,mem[lcl]); // read the cache line from memory
					mem_access++ ;
					DE.set_owner(0,net_cmd.src);
					DE.set_dirty(1);
					DE.set_counter(0);
					set_dir_entry( pc.addr, DE.entry );
					send_ack(net_cmd.src ,pc);     
				}
			}
			else
			{
				my_status.last_inv_tag = cache->gen_address_tag(pc.addr);
				my_status.last_inv_time = 0;
				//not HS but received an Invalidate
				if(cache->cache_access(pc.addr,SHARED,&car)) // invalidate the cache line if present
				{
					//If in MODIFIED, send back data
					//if(cache->cache_access(pc.addr,MODIFIED,&car)) {
						data_t cache_line;
						read_cache_line( pc.addr, car, cache_line);
					    print_cache_line( pc.addr, car);
						copy_cache_line(pc.data, cache_line);
					//}
					cache->modify_permit_tag(car,INVALID);
				}
				NOTE_ARGS(("Got Invalidate req from HS %d to %d\n", net_cmd.src, node)); 
                        
                //printf(" shared data sent back as ack ");
                //for (int i = 0; i < CACHE_LINE_SIZE; ++i) printf("%d ",pc.data[i]);  //TODO remove later
                //printf("\n ");
			
                        send_ack(net_cmd.src,pc);
			}
			return true;
	}
}


//Return 1 if sent a message in this function, return false
bool iu_t::process_net_reply(net_cmd_t net_cmd) {
	proc_cmd_t pc = net_cmd.proc_cmd;
	NOTE_ARGS(("node: %d net reply about address %x, src = %d\n", node, pc.addr, net_cmd.src));
	switch(pc.busop) {
		case READ: 
			// Local address -> Got an inv ack, either for me or someone else
			//    .... update directory ...
			// 	if done receiving all acks, send the resp to dst
			// Global_address -> Whatever I am waiting on, for proc_request
			//     proc_cmd_p to false and cache_reply
			if(gen_node(pc.addr)  == node) {
				//Got back an INV ACK. Was my proc waiting on it?
				//Only check if the proc_cmd is actually being processed already
				dir_entry DE = get_dir_entry(pc.addr);				
				assert(DE.get_inv_acks_waiting());
				NOTE_ARGS(("Got back inv_ack from proc %d to proc %d\n", net_cmd.src, node));
				if((proc_cmd_processed_p) && (proc_cmd.addr == pc.addr) && (proc_cmd.tag == pc.tag)) {
					//This was a response to my proc's STORE op or a READ on dirty line
					int I_am_done = 0;
					if(DE.get_dirty()) {
						I_am_done = 1;
						int lcl = gen_local_cache_line(pc.addr);
						copy_cache_line(mem[lcl], pc.data);
						mem_access++ ;
					}
					else {
						int cur_count = DE.get_counter();
						if(cur_count == 5) {
							cur_count = DE.get_owner(3);
							cur_count--;
							DE.set_owner(3, cur_count);
							assert(cur_count >= 0);
						}
						else {
							cur_count--;
							assert(cur_count >= 0);
							DE.set_counter(cur_count);
						}
						if(cur_count == 0) {
							I_am_done = 1;
						}
					}
					if(I_am_done) {
						proc_cmd_p = false; // clear out request that this reply is a reply to
						//Invalidated the MOD owner, WB data to mem
						int lcl = gen_local_cache_line(pc.addr);
						copy_cache_line(mem[lcl], pc.data);
						mem_access++ ;
						//Set the directory entries
						//Was it a STORE req?
						if(pc.permit_tag == MODIFIED) 
							DE.set_dirty(1);
						else
							DE.set_dirty(0);
						DE.set_counter(0);
						DE.set_self_ownership(1);
						DE.set_inv_acks_waiting(0);
						set_dir_entry(pc.addr, DE.entry);
						//Set the cache with the received stuff
						cache->reply(pc);
					}
					else
						set_dir_entry(pc.addr, DE.entry);
					return(false);

				}
				else {
					int I_am_done = 0;
					//This was an INV ack for someone else's READ/INVALIDATE request
					//Make sure I am waiting on INV ACKS
					assert(DE.get_inv_acks_waiting());
					//Make sure I am invalidated
					assert(!DE.get_self_ownership());
					//Make sure the ACK is not from the requester
					assert(DE.get_destination() != net_cmd.src);

					if(DE.get_dirty()) {
						//Got back data from modified owner, write it back
						I_am_done = 1;
						int lcl = gen_local_cache_line(pc.addr);
						copy_cache_line(mem[lcl], pc.data);
                        
                        //printf(" inv ack data mem write ");
                        //for (int i = 0; i < CACHE_LINE_SIZE; ++i) printf("%d ",pc.data[i]);  //TODO remove later
                        //printf("\n ");


						mem_access++ ;
					}		
					else {
						int cur_count = DE.get_counter();
						if(cur_count == 5) {
							cur_count = DE.get_owner(3);
							cur_count--;
							DE.set_owner(3, cur_count);
							assert(cur_count >= 0);
						}
						else {
							cur_count--;
							assert(cur_count >= 0);
							DE.set_counter(cur_count);
						}
						if(cur_count == 0) {
							I_am_done = 1;
						}
					}
					if(I_am_done) {
						//Done receiving all INV ACKS for this address
						DE.set_inv_acks_waiting(0);
						if (pc.permit_tag == MODIFIED) {
							//Request was an INVALIDATE req, set to dirty
							DE.set_dirty(1);
							DE.set_counter(0);
							DE.set_owner(0,DE.get_destination());
						}
						else {
							//Request was a READ req, set to clean
							DE.set_dirty(0);
							DE.set_counter(1);
							DE.set_owner(0,DE.get_destination());
						}
						//Send message to the requester	
						send_ack(DE.get_destination(), pc);
						set_dir_entry(pc.addr, DE.entry);
						return(true);
					}
					set_dir_entry(pc.addr, DE.entry);
					return(false);

				}

			}
			//HS ACK received, should ideally be what my proc is waiting on
			else {
				if(proc_cmd.tag != pc.tag) {
					printf("ERROR!! Received an old ACK???\n");
					return(false);
				}
				proc_cmd_p = false;
				//Rest will be taken care of by cache_reply, 
				//since the wished for permit tag is a part of proc_cmd
				//Make sure all acks are reusing the proc_cmd_t
				cache->reply(pc);	
			}	
			return(false);

		case WRITE: // we should not get any WRITE reply
			ERROR("should not have gotten a reply back from a write or an invalidate, since we are incoherent");
			assert(0);
			return(false);  // need to return something for now
		case INVALIDATE:  // we should not get any INVALIDATE reply
			ERROR("should not have gotten a reply back from a write or an invalidate, since we are incoherent");
			assert(0);
			return(false);  // need to return something for now
	}
}

void iu_t::print_stats()
{
	printf("------------------------------\n");
	printf("%d: iu\n", node);

	printf("num local  accesses = %d\n", local_accesses);
	printf("num global accesses = %d\n", global_accesses);
}

bool iu_t::to_net()
{
	bool ret = 0;

	if(!my_status.reply_send_queue->empty()) {

		send_packet_type next_send = my_status.reply_send_queue->front();
		assert(next_send.cmd.dest!=next_send.cmd.src);	
		ret = net->to_net( next_send.src_port, next_send.pri, next_send.cmd );
		if(!ret) {
			NOTE_ARGS(("failed the send at node %d to the queue %d\n", node, next_send.pri));
		}
		else {
			my_status.reply_send_queue->pop();	
		}

	}			
	else if(!my_status.send_queue->empty()) {
		send_packet_type next_send = my_status.send_queue->front();
		assert(next_send.cmd.dest!=next_send.cmd.src);
		ret = net->to_net( next_send.src_port, next_send.pri, next_send.cmd );
		if(!ret) {
			NOTE_ARGS(("failed the send at node %d to the queue %d\n", node, next_send.pri));
		}
		else {
			my_status.send_queue->pop();	
		}

	}			
	return ret;
}


void iu_t::send_retry( int dest, proc_cmd_t proc_cmd )
{
	net_cmd_t net_cmd;
	net_cmd.dest = dest;
	net_cmd.src = node;
	net_cmd.proc_cmd = proc_cmd;
	net_cmd.proc_cmd.busop = WRITE;
	send_packet_type packet(net_cmd, node, REQUEST);	
	if(my_status.send_queue->full_p())
		assert(0);
	my_status.send_queue->push_back(packet);
	//to_net(node,REQUEST,net_cmd);

}

void iu_t::send_ack( int dest, proc_cmd_t proc_cmd )
{
	net_cmd_t net_cmd;
	net_cmd.dest = dest;
	net_cmd.src = node;
	net_cmd.proc_cmd = proc_cmd;
	net_cmd.proc_cmd.busop = READ;
	send_packet_type packet(net_cmd, node, REPLY);	
	if(my_status.reply_send_queue->full_p())
		assert(0);
	my_status.reply_send_queue->push_back(packet);
	//	to_net(node,REPLY,net_cmd);

}


void iu_t::send_command( int dest, proc_cmd_t proc_cmd )
{
	net_cmd_t net_cmd;
	net_cmd.dest = dest;
	net_cmd.src = node;
	net_cmd.proc_cmd = proc_cmd;
	send_packet_type packet(net_cmd, node, REQUEST);	
	if(my_status.send_queue->full_p())
		assert(0);
	my_status.send_queue->push_back(packet);
	//	to_net(node,REQUEST,net_cmd);

}

void iu_t::broadcast_invalidate(proc_cmd_t pc) // broadcast invalidates - one at a time
{
	pc.busop = INVALIDATE;
	dir_entry DE = get_dir_entry( pc.addr );
	int dest = my_status.ISC;

	my_status.last_ISC_cmd.proc_cmd = pc;
	if( (dest == node) || dest == DE.get_destination() ) // check two times, HS and requester
	{
		my_status.ISC--;
		if(my_status.ISC < 0)
			return;
		dest = my_status.ISC;

		if( (dest == node) || dest == DE.get_destination() )
		{
			my_status.ISC--;
			if(my_status.ISC < 0)
				return;
			dest = my_status.ISC;
		}
	}

	if( my_status.ISC >= 0) // invalidates still to be sent
	{
		send_command( dest, pc );  // ISC - Invalidate send count ( count down)
		my_status.ISC--;
	}
	else
		my_status.broadcast = false;
}

void iu_t::send_invalidate(proc_cmd_t cmd)
{
	dir_entry DE = get_dir_entry( cmd.addr );
	int dest = DE.get_owner( my_status.ISC );
	cmd.busop = INVALIDATE;
	my_status.last_ISC_cmd.proc_cmd = cmd;
	NOTE_ARGS(("MY_ISC was set to %d Node: %d Current_dest: %d Dest: %d Address: %x\n", my_status.ISC, node, dest, DE.get_destination(), cmd.addr));

	if(my_status.ISC < 0) {
		my_status.broadcast = false;
		return;
	}

	if(dest == node || dest == DE.get_destination() ) // check two times, HS and requester
	{ 
		if(dest == DE.get_destination())
			my_status.ISC--;
		if(my_status.ISC < 0) {
			NOTE_ARGS(("TRAPPED\n"));
			my_status.broadcast = false;
			return;
		}
		dest = DE.get_owner( my_status.ISC );

		if(dest == node || dest == DE.get_destination())
		{ 
			if(dest == DE.get_destination())
				my_status.ISC--;
			if(my_status.ISC < 0) {
				my_status.broadcast = false;
				return;
			}

			dest = DE.get_owner( my_status.ISC );
		}    
	}

	if( my_status.ISC >= 0) // invalidates still to be sent
	{
		send_command( dest, cmd );
		my_status.ISC--;
	}
	else
		my_status.broadcast = false;
}

bool iu_t::check_retry(net_cmd_t net_cmd)
{   
	dir_entry DE = get_dir_entry( net_cmd.proc_cmd.addr );
	if( DE.get_inv_acks_waiting() )
	{   
		send_retry(net_cmd.src,net_cmd.proc_cmd);  // retry sent back to the src
		return 1;
	}
	return 0;

}
void iu_t::self_retry() { 
	//to_net( my_status.last_src_port, my_status.last_type, my_status.last_net_cmd ); 
}
